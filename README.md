This is a demonstration and development test bench for abivia/nextform-laravel,
a bridge between our NextForm form generator and Laravel.

The application should run, although this is a goal, not a guarantee.

We haven't changed the home page yet. After setting up a database and running
migrations, try {your root}/tests to get started.

We'll add more to this over time.