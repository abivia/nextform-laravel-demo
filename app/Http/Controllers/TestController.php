<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Test;

class TestController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        app('nextform')->addSchema('test-schema');

        return view('tests/create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $test = Test::findOrFail($id);
        $test->delete();

        return redirect('/tests')->with('success', 'Test is successfully deleted');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test = Test::findOrFail($id);
        app('nextform')->addSchema('test-schema');
        app('nextform')->populate($test);

        return view('tests/edit', compact('test'));
    }

    public function editNf($id)
    {
        // fetch the record
        // create a NF object
        // add schemas
        // populate data
        return view('bladefile', $nfObject);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::all();

        return view('tests/index', compact('tests'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'key' => 'required|max:255',
            'checkuser' => 'simplecaptcha|required',
            'value' => 'required|max:255',
        ]);
        $test = Test::create($validatedData);

        return redirect('/tests')->with('success', 'Test is successfully saved');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'key' => 'required|max:255',
            'value' => 'required|max:255',
        ]);
        Test::whereId($id)->update($validatedData);

        return redirect('/tests')->with('success', 'Test is successfully updated');
    }

}
