<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Validator::extend(
            'simplecaptcha',
            function ($attribute, $value, $parameters, $validator) {
                // Match the value in the session
                $context = session('NextForm');
                $user = (int) $value;
                $valid = $context['Abivia\NextForm\Captcha\SimpleCaptcha']['answer'];
                $result = $user === $valid;
                return $result;
            },
            trans('Please try again.')
        );
    }
}
