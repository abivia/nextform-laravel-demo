<?php

/**
 * Form language strings
 */
return [
    'test.key' => 'Key:',
    'test.required' => 'Sorry, this can\'t be empty.',
    'test.value' => 'Any value:',
];

