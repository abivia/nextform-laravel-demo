@extends('../layout')
<?php NextForm::generate('test-form', ['action' => route('tests.store')]); ?>

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Add Tests
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    <?php NextForm::body(); ?>
  </div>
</div>
@endsection
@section('pagelinks')
<?php NextForm::links(); ?>
@endsection
@section('styles')
<style>
<?php NextForm::styles(); ?>
</style>
@endsection
@section('script')
<?php NextForm::scriptFiles(); ?>
<script>
<?php NextForm::script(); ?>
</script>
@endsection
