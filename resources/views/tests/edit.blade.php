@extends('../layout')
<?php //xdebug_break(); ?>
<?php
NextForm::generate(
    'test-form',
    [
        'action' => route('tests.update', $test->id),
        'state' => ['_method' => 'PATCH']
    ]
);
?>
@section('styles')
<style>
  .uper {
    margin-top: 40px;
  }
<?php NextForm::styles(); ?>
</style>
@endsection
@section('content')
<div class="card uper">
  <div class="card-header">
    @lang('Update Tests')
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    <?php NextForm::body(); ?>
  </div>
</div>
@endsection
@section('pagelinks')
<?php NextForm::links(); ?>
@endsection
@section('script')
<?php NextForm::scriptFiles(); ?>
<script>
<?php NextForm::script(); ?>
</script>
@endsection
