@extends('../layout')

@section('content')
<style>
  .upper {
    margin-top: 40px;
  }
</style>
<div class="upper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div><br />
  @endif
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Test Key</td>
          <td>Test Value</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($tests as $test)
        <tr>
            <td>{{$test->id}}</td>
            <td>{{$test->key}}</td>
            <td>{{$test->value}}</td>
            <td><a href="{{ route('tests.edit', $test->id)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('tests.destroy', $test->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
  <a href="{{ route('tests.create')}}" class="btn btn-primary">Create</a>
<div>
@endsection